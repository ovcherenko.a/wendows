import { combineReducers } from 'redux';
import { applicationReducer } from './applications/reducers'
// import { runningApplicationsReducee } from './runningApplications/reducers'

export default combineReducers({ 
  applicationReducer: applicationReducer,
//   runningApplicationsReducee: runningApplicationsReducee
})