import React from 'react';
import WindowWrapper from './../../components/WindowWrapper/WindowWrapper';
import Calculator from './../../components/WindowWrapper/inWindowApps/Calculator/Calculator';
import Minesweeper from './../../components/WindowWrapper/inWindowApps/Minesweeper/Minesweeper';

import {    ACTION_RUN_NEW_APP, 
            ACTION_RUN_CALCULATOR_APP, 
            ACTION_STOP_CALCULATOR_APP,
            ACTION_RUN_MINESWEEPER_APP,
            runCalculatorApp,
            runMinesweeperApp } from './actions';


const defaultState = {
    applicationList: [
        <WindowWrapper 
            name={'Calculator'}
            runApp={runCalculatorApp}>
            <div><Calculator/></div>
        </WindowWrapper>,
        <WindowWrapper
            name={'Minesweeper'}
            runApp={runMinesweeperApp}>
            <div><Minesweeper/></div>
        </WindowWrapper>
    ],
    runningApplications: [

    ]
}

export const applicationReducer = (state = defaultState, action) => {
    let newRunningApplications = [];
    switch (action.type) {
        case ACTION_RUN_NEW_APP:
            newRunningApplications = state.runningApplications.slice();
            newRunningApplications.push(action.payload);
            return {
                ...state,
                runningApplications: newRunningApplications
            }
        case ACTION_RUN_MINESWEEPER_APP: 
            console.log('action run minesweeper')
            newRunningApplications = state.runningApplications.slice();
            if(!isAppAlreadyRunning(newRunningApplications, action.payload.props.name)) {
                newRunningApplications.push(action.payload);
                return {
                    ...state,
                    runningApplications: newRunningApplications
                } 
            } else {
                return state
            }
        case ACTION_RUN_CALCULATOR_APP: 
            console.log('action run calc')
            newRunningApplications = state.runningApplications.slice();
            if(!isAppAlreadyRunning(newRunningApplications, action.payload.props.name)) {
                newRunningApplications.push(action.payload);
                return {
                    ...state,
                    runningApplications: newRunningApplications
                } 
            } else {
                return state
            }
        case ACTION_STOP_CALCULATOR_APP:
            console.log('CTION_STOP_CALCULATOR_APP')
            newRunningApplications = state.runningApplications.slice();
            newRunningApplications.forEach( (oneApp, index) => {
                if(oneApp.props.name === action.payload) {
                    newRunningApplications.splice(0, 1)
                }
            })
            return {
                ...state,
                runningApplications: newRunningApplications
            }
        default: {
            console.log('reduser default case')
            return state
        }
    }
    
    return state
}

function isAppAlreadyRunning(allApps, testingAppName) {
    let appAlreadyRunning = false;
    allApps.forEach(app => {
        if (app.props.name === testingAppName) {
            appAlreadyRunning = true
        } 
    })

    return appAlreadyRunning
}