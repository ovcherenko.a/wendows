import React from 'react';
import WindowWrapper from '../../components/WindowWrapper/WindowWrapper';


//Apps
import Calculator from './../../components/WindowWrapper/inWindowApps/Calculator/Calculator'
import Minesweeper from './../../components/WindowWrapper/inWindowApps/Minesweeper/Minesweeper'

export const ACTION_RUN_NEW_APP = 'ACTION_RUN_NEW_APP';
export const ACTION_RUN_CALCULATOR_APP = 'ACTION_RUN_CALCULATOR_APP';
export const ACTION_STOP_CALCULATOR_APP = 'ACTION_STOP_CALCULATOR_APP';
export const ACTION_RUN_MINESWEEPER_APP = 'ACTION_RUN_MINESWEEPER_APP';

export const runCalculatorApp = () => {
    console.log('clicked calc action')
    return {
        type: ACTION_RUN_CALCULATOR_APP,
        payload: (<WindowWrapper name="Calculator"> <Calculator/> </WindowWrapper>),
    }
}

export const runMinesweeperApp = () => {
    console.log('clicked minesweeper app')
    return {
        type: ACTION_RUN_MINESWEEPER_APP,
        payload: (<WindowWrapper name="Minesweeper"> <Minesweeper/> </WindowWrapper>)
    }
}

export const delApp = (name) => {
    console.log('deleting apps')
    console.log(name)
    return {
        type: ACTION_STOP_CALCULATOR_APP,
        payload: name
    }
}

export const runNewApp = (appToRun) => {
    return { 
        type: ACTION_RUN_NEW_APP,
        payload: (<WindowWrapper> {appToRun} </WindowWrapper>)
    }
}