import React, {useEffect, useState} from 'react'
import './BarClock.scss'

const BarClock = () => {

    const [currentTime, setCurrentTime] = useState(undefined);

    const getDate = () => { 
        let date = new Date();
        let hours = ('0' + date.getHours()).slice(-2);
        let minutes = ('0' + date.getMinutes()).slice(-2);
        return ( `${hours}:${minutes}`)
    }    

    useEffect(() => {
        setCurrentTime(getDate())
        setInterval(()=> { 
            setCurrentTime(getDate())
        }, 10000)
    }, [])

    return (
            <span className="bar-clock">{currentTime}</span>
    )
}

export default BarClock