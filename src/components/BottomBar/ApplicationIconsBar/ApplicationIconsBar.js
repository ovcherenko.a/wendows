import React from 'react';
import './ApplicationIconsBar.scss'

import BarClock from './BarClock/BarClock.js'

const ApplicationIconsBar = () => {

    return (
        <div className="application-icons-bar">
            <div className="icons-wrapper">
                <img src="./assets/icons/volume-1.png" alt="sound icon"/>
            </div>
            <span><BarClock/></span>
        </div>
    )
}

export default ApplicationIconsBar