import React from 'react';
import './BottomBar.scss'

import StartButtom from './StartButton/StartButton.js';
import ApplicationListBar from './ApplicationListBar/ApplicationListBar.js';
import ApplicationIconsBar from './ApplicationIconsBar/ApplicationIconsBar.js';

const BottomBar = () => {

        return (
            <div className="bottom-bar">
                <StartButtom/>
                <ApplicationListBar/>
                <ApplicationIconsBar/>   
            </div>
    )
}

export default BottomBar