import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {    runNewApp, 
            runCalculatorApp,
            runMinesweeperApp } from './../../../../store/applications/actions'
import './StartApplicationList.scss'

const StartApplicationList = ({applications,
                                startListOpenHandler,
                                runCalculatorAppDisp,
                                runMinesweeperAppDisp,
                                applicationListOpenHandlerTrue, 
                                applicationListOpenHandlerFalse}) => {
    const applicationList = [];

    applicationList.push( <button onClick={runCalculatorAppDisp}> {applications[0].props.name}</button> )
    applicationList.push( <button onClick={runMinesweeperAppDisp}> {applications[1].props.name}</button> )

    // applications.forEach(element => {
    //     // console.log(element.props.runApp)
    //     applicationList.push( <button onClick={runCalculatorAppDisp}> {element.props.name}</button> )
    // });

    return (
        <div className='start-application-list-wrapper'
            onMouseEnter={applicationListOpenHandlerTrue}
            onMouseLeave={applicationListOpenHandlerFalse}>
            {applicationList}
        </div>
    )
}

const putStateToProps = (state) => {
    return {
        applications: state.applicationReducer.applicationList
    }
}

const putActionsToProps = (dispatch) => {
    return {
        runCalculatorAppDisp: bindActionCreators(runCalculatorApp, dispatch),
        runMinesweeperAppDisp: bindActionCreators(runMinesweeperApp, dispatch)
    }
}

export default connect(putStateToProps, putActionsToProps)(StartApplicationList)