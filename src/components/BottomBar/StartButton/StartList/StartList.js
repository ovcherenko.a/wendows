import React, { useState } from 'react'
import StartApplicationList from './../StartApplicationList/StartApplicationList'
import './StartList.scss'

const StartList = ({startListOpenHandler}) => {
    const [applicationListOpen, setApplicationListOpen ] = useState(false)

    const applicationListOpenHandlerTrue = () => {
        setApplicationListOpen(true);
    }

    const applicationListOpenHandlerFalse = () => {
        setApplicationListOpen(false);
    }
    
    return (
        <div className="start-menu">
            {applicationListOpen ? <StartApplicationList
                applicationListOpenHandlerTrue = {applicationListOpenHandlerTrue}
                applicationListOpenHandlerFalse = {applicationListOpenHandlerFalse}
            /> : ''}
            <div className="left-title">
                <p>Wendows<span className="title-number ">20</span></p>
            </div>
            <div className="start-list">
                <ul>
                    <li>
                        <button 
                            className="additional-list"
                            onMouseEnter={applicationListOpenHandlerTrue}
                            onMouseLeave={applicationListOpenHandlerFalse}>
                            Programs
                        </button>
                    </li>
                    <li>
                        <button className="additional-list">
                            Documents
                        </button>
                    </li>
                    <li>
                        <button>
                            Settings
                        </button>
                    </li>
                    <li>
                        <button>
                            Shut down
                        </button>
                    </li>
                </ul>
            </div>
        </div>
    )
}


export default StartList