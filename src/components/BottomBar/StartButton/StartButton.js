import React, { useState } from 'react';
import StartList from './StartList/StartList'
import './StartButton.scss'

const StartButton = () => {
    const [isStartListOpen, setIsStartListOpen] = useState(false)

    const startListOpenHandler = () => {
        setIsStartListOpen(!isStartListOpen);
    }

    return (
        <>
            {isStartListOpen ? <StartList startListOpenHandler={startListOpenHandler}/> : ''}
            <button className={`start-button-wrapper ${isStartListOpen ? 'activeStartButton' : ''}`}
                    onClick={startListOpenHandler}>
            {/* <button className="start-button-wrapper"> */}
                <span>Start</span>
            </button>
        </>
    )
}

export default StartButton;