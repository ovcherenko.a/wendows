import React from 'react';
import './ApplicationListBar.scss'

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { runNewApp } from './../../../store/applications/actions'

const ApplicationListBar = ({ runningApplications }) => {
    const applicationElements = [];
    
    runningApplications.forEach(element => {
        applicationElements.push(<div className="one-application-wrapper">{element.props.name}</div>)
    });
    return (
        <div className="application-list-bar">
            {applicationElements}
        </div>
    )
}

const putStateToProps = (state) => {
    return {
        runningApplications: state.applicationReducer.runningApplications
    }
}

const putActionsToProps = (dispatch) => {
    return {
        runNewApp: bindActionCreators(runNewApp, dispatch)
    }
}

export default connect(putStateToProps, putActionsToProps)(ApplicationListBar);