import React, {useEffect, useState} from 'react'
import "./Minesweeper.scss"

import OneSquare from './OneSquare/OneSquare'



const Minesweeper = () => {
    const [gameStatusState, setGameStatusState] = useState(null);
    const [gameContentState, setGameContentState] = useState(null);
    const [renderCounts, setRenderCounts] = useState(0);

    const renderBlocks = (status) => {
        let gameContent = []; 

        if( status !== null && status !== undefined) {
            for (let i = 0; i < status.length; i++) {
                let row = [];
                for (let ii = 0; ii < status[i].length; ii++) {
                    row.push(
                        <OneSquare 
                            key = {Math.random()}
                            mode={status[i][ii]}
                            y={i}
                            x={ii}
                        />
                    )
                }
                gameContent[i] = getRow(row);
            }
        }
        return gameContent
    }


    const countingBombs = (status) => {
        // let gameState = gameStatusState.slice();
        console.log('counting bombs')

        console.log('status', status)

        // status.forEach(item => {
        //     console.log(item)
        // })
        if(status && status.length > 0) {
            for(let i = 0; i < status.length; i++) {
                for(let ii = 0; ii < status[i].length; ii++) {
                    console.log('ololo')

                    if(i === 0 && ii === 0 && status[i][ii].bomb) {
                        status[i+1][ii+1].number = status[i+1][ii+1].number+1
                        status[i][ii+1].number = status[i][ii+1].number+1
                        status[i+1][ii].number = status[i+1][ii].number+1
                    }
                    
                    if(i === status.length && ii === status.length && status[i][ii].bomb) {
                        status[i-1][ii-1].number = status[i-1][ii-1].number+1
                        status[i][ii-1].number = status[i][ii-1].number+1
                        status[i-1][ii].number = status[i-1][ii].number+1
                    }

                    if ( i === 0 && ii < 1 && ii > status[i].length-1 && status[i][ii].bomb) {
                        status[i+1][ii+1].number = status[i+1][ii+1].number+1
                        
                    }

                    if ( i < 1 && ii === 0 && status[i][ii].bomb ) {

                    }

                    if (i > 0 && 
                        ii > 0 && 
                        i < status.length-1 && 
                        ii < status.length-1 && 
                        status[i][ii].bomb) {
                            status[i+1][ii+1].number = status[i+1][ii+1].number+1
                            status[i][ii+1].number = status[i][ii+1].number+1
                            status[i+1][ii].number = status[i+1][ii].number+1
                            status[i-1][ii-1].number = status[i-1][ii-1].number+1
                            status[i][ii-1].number = status[i][ii-1].number+1
                            status[i-1][ii].number = status[i-1][ii].number+1
                            status[i+1][ii-1].number = status[i+1][ii-1].number+1;
                            status[i-1][ii+1].number = status[i-1][ii+1].number+1;
                    }
                }
            }
        }
        console.log('STATUS!!!', status)
        return status;
    }

    const renderGameStatusN = () => {
        let tempGameStatus = [];

        let count = 0;
        for(let i = 0; i < 8; i++) {
            let row = [];
            for(let ii = 0; ii < 8; ii++) {
                let num = Math.floor(0 - 0.5 + Math.random() * (1 - 0 + 1));
                if (num === 1 && count < 5) {
                    row.push({
                        open: 0,
                        bomb: true,
                        number: 0
                    })
                    count++
                } else {
                    row.push({
                        open: 0,
                        bomb: false,
                        number: 0
                    })
                }
            }
            tempGameStatus.push(row)
        }
        return tempGameStatus
    }

    const changeSquareMode = (y, x, mode) => {
        // let gameState = gameStatusState.slice();
        // gameState[y][x] = {
        //     open: 1,
        //     bomb: gameStatusState[y][x].bomb,
        //     number: gameStatusState[y][x].number
        // };
        // setGameStatusState(gameState);
        // setGameContentState( renderBlocks(gameState) )
    
        gameContentState[1].props.children[1].props.mode.open = 1

        console.log(gameContentState)
        console.log(gameContentState[1].props.children[1].props.mode)
    }

    const restartGame = () => {
        const newGameStatus = renderGameStatusN();
        setGameStatusState(newGameStatus);
        setGameContentState( renderBlocks(newGameStatus) )
    }

    const getRow = (item) => {
        return (
            <div className="minesweeper-wrapper__game-wrapper__horizontal">
                {item}
            </div>
        )
    }
    

    useEffect( () => {
        setGameStatusState( renderGameStatusN() )
    }, [])

    useEffect( () => {
        let tempStatusState = gameStatusState;
        let newRenderCounts = renderCounts;
        if (newRenderCounts < 2) {
            tempStatusState = countingBombs(tempStatusState)
            console.log('renderCounst', renderCounts)
        }
        newRenderCounts++;
        setRenderCounts(newRenderCounts)

        setGameContentState( renderBlocks(tempStatusState) )
    }, [gameStatusState])


    const clickHangler = (e) => {
        // console.log(e.target.dataset)
        const x = e.target.dataset.x;
        const y = e.target.dataset.y;
        if(x !== undefined || y !== undefined) {
            changeSquareMode(y, x, 1)
        }
    }

    return (
        <div className="minesweeper-wrapper">
            <div className="minesweeper-wrapper__status-wrapper">
                <div className="minesweeper-wrapper__status-wrapper__numeral">
                    <p>099</p>
                </div>
                <div>
                    <button onClick={restartGame}>
                        r
                    </button>
                </div>
                <div className="minesweeper-wrapper__status-wrapper__numeral">
                    <p>999</p>
                </div>
            </div>
            <div 
                className="minesweeper-wrapper__game-wrapper"
                onClick={clickHangler}
            >

                {gameContentState}

            </div>
        </div>
    )
}

export default Minesweeper