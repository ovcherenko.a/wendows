import React, {useEffect, useState, useMemo} from 'react'
import './OneSquare.scss'

const OneSquare = ({mode, clickSquareHandler, y, x}) => {
    const [numberCard, setNumberCard] = useState(null);
    const [modeCard, setModeCard] = useState(0);
    const [showBomb, setShowBomb] = useState(false);

    useEffect(() => {
        if ( mode.open && mode.bomb === true ) {
            setShowBomb(true)
        } else if ( mode.open && mode.number > 0 ) {
            setNumberCard(mode.number)
        }
    }, [mode])


    return(
        <div 
            className={'card card-'+mode.open}
            // className={'card card-bomb'}
            data-y={y}
            data-x={x}
            // onClick={clickSquareHandler.bind(null, setModeCard, y, x)}
            // onClick={() => {
            //     clickSquareHandler(y, x);
            // }}
        >
            {/* <p className="blue">{mode.open > 0 ? mode.number : ''}</p> */}

            {/* {showBomb ? <img src='./assets\icons\minesweeper/bomb.png' alt='bomb icon'/> : ''} */}

            {showBomb ? <img src='./assets\icons\minesweeper/bomb.png' alt='bomb icon'/> : <p className="blue">{numberCard}</p>}
        
        </div>
    )
}

export default OneSquare;