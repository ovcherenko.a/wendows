import React, {useState, useRef} from 'react'
import './Calculator.scss'

const Calculator = () => {
    // const [sum, setSum] = useState(0);
    const [tempNumber, setTempNumber] = useState(0);
    const [arithmeticOperator, setArithmeticOperator] = useState('')
    const [numberInInput, setNumberInInput] = useState('');
    
    
    // let refContainer = useRef()
    
    const clickHandler = (e) => {
        let num = e.target.innerHTML;

        if(num.length === 1) {
            if ( num >= 0 && num < 10 || num === '.') {
                if (num === '.' && !numberInInput.includes('.')) {
                    setNumberInInput(numberInInput + num);
                } else if (num >= 0 && num < 10) {
                    setNumberInInput(numberInInput + num)
                }
            } else if ( num === "+" || 
                        num === "*" || 
                        num === "/" || 
                        num === "-") {
                setArithmeticOperator(num);
                // setSum( eval(+numberInInput + num + +tempNumber) );
                setTempNumber(+numberInInput);
                setNumberInInput('') 
            } else if (num === "=") {
                setNumberInInput( eval(+tempNumber + arithmeticOperator + +numberInInput ) )
            }
        }
    } 

    const resetCalc = () => {
        setNumberInInput('')
    }

    return (
        <div className="calculator-wrapper">
            <div className="input-wrapper">
                <input type="text" placeholder="0" value={numberInInput} readOnly/>
            </div>
            <div className="control-buttons-wrapper">
                {/* <button>BACK</button> */}
                {/* <button>CE</button> */}
                <button onClick={resetCalc}>C</button>
            </div>
            <div className="numbers-wrapper" onClick={clickHandler}>
                <button>7</button>
                <button>8</button>
                <button>9</button>
                <button>/</button>
                {/* <button>sqrt</button> */}

                <button>4</button>
                <button>5</button>
                <button>6</button>
                <button>*</button>
                {/* <button>%</button> */}

                <button>1</button>
                <button>2</button>
                <button>3</button>
                <button>-</button>
                {/* <button>1/x</button> */}

                <button>0</button>
                <button>.</button>
                <button>+</button>
                <button>=</button>
                {/* <button>sqrt</button> */}
            </div>
        </div>
    )
}

export default Calculator