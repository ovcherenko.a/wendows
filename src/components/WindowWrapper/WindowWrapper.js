import React, {useRef, useEffect, useState} from 'react';
import { connect } from 'react-redux';
import Draggable from 'react-draggable';
import './WindowWrapper.scss';
import { delApp } from './../../store/applications/actions'
import { bindActionCreators } from 'redux';

const WindowWrapper = ({children, name, applicationStateHandler, delApp}) => {
    const [wrapperWidth, setWrapperWidth] = useState(0);
    const [wrapperHeight, setWrapperHeight] = useState(0);

    const handleStart = (e) => {
        if(e.target.className.slice(0, 6) !== 'header') {
            return false
        }
    }
    
    const wrapper = useRef({})

    useEffect( () => {
        setWrapperHeight(wrapper.current.clientHeight);
        setWrapperWidth(wrapper.current.clientWidth);
    }, [])

    return (
        <Draggable
            bounds={{
                left:0, 
                top: 0, 
                right: window.innerWidth - wrapperWidth - 4,
                bottom: window.innerHeight - wrapperHeight - 29
            }}
            onStart={handleStart}>
            <div 
                className="window-wrapper"
                ref={wrapper}>
                <div 
                    className="header header-1">
                    <div>
                        <div className="icon"></div>
                        <p className="title">{name}</p>
                    </div>

                    <div className="button-list" onClick={applicationStateHandler}>
                        <button data-function='minimize'>
                            <img src="./assets/icons/window-minimize-icon.png" alt="minimize"/>
                        </button>
                        <button data-function='expand'>
                            <img src="./assets/icons/window-expand-icon.png" alt="expand"/>
                        </button>
                        <button data-function='close' onClick={()=>{
                            delApp(name)}}>
                            <img src="./assets/icons/window-close-icon.png" alt="close"/>
                        </button>
                    </div>
                </div>

                <div className="options-list">
                    <button>
                        File
                    </button>
                    <button>
                        Help
                    </button>
                </div>

                <div className="content">
                    {children}
                </div>
            </div>
        </Draggable>
    )
}

const putStateToProps = (state) => {
    return {
        applications: state.applicationReducer.applicationList
    }
}

const putActionsToProps = (dispatch) => {
    return {
        delApp: bindActionCreators(delApp, dispatch)
    }
}

export default connect(putStateToProps, putActionsToProps)(WindowWrapper);