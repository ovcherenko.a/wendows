import React, { useState, useEffect } from 'react';
import './MainBackground.scss'

const MainBackground = ( {children}) => {
    const [ypos, setYpos] = useState(`0px`);
    const [xpos, setXpos] = useState(`0px`);
    const [isContextMenuOpen, setIsContextMenuOpen] = useState(false)

    useEffect(() => {
        

        let leftClick = (e) => {
            e.preventDefault();
            if (e.target.className === 'main-background' ||
                e.target.className === 'context-menu' ||
                e.target.localName === 'button'){
                setIsContextMenuOpen(false)
            }
        }
        document.addEventListener('click', leftClick)
        
        let rightClick = (e) => {

            e.preventDefault();
            if(e.target.className === 'main-background') {
                setYpos(`${e.y}px`);
                setXpos(`${e.x}px`);
    
                setIsContextMenuOpen(!isContextMenuOpen);
            }
        }
        document.addEventListener('contextmenu', rightClick)

        return () => {
            document.removeEventListener('click', leftClick)
            document.removeEventListener('contextmenu', rightClick)
        }
    }, [])
    

    return(
        <div className="main-background">
            {isContextMenuOpen ? 
            <div className='context-menu' style={{
                left: xpos,
                top: ypos
            }}>
                <button>Properties</button>
            </div> : ''
            }
            {children}
        </div>
    )
}

export default MainBackground; 
