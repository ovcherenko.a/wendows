import React from 'react';
import BottomBar from './components/BottomBar/BottomBar.js'
import MainBackground from './components/MainBackground/MainBackground.js'
import Calculator from './components/WindowWrapper/inWindowApps/Calculator/Calculator'

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { runNewApp, runCalculatorApp } from './store/applications/actions'

import './App.css';

function App({runningApplications, runNewApp}) {

  // console.log(runningApplications)
 
  return (
    <div className="App">
      {/* <button onClick={() => {runNewApp(<Calculator/>)}}>dispatch add app</button> */}

      {runningApplications}
  
      <MainBackground>
      </MainBackground>
      <BottomBar/>
    </div>
  );
}

// const mapStateToProps = (state) => {
  const putStateToProps = (state) => {
    console.log(state)
    return {
      runningApplications: state.applicationReducer.runningApplications
    }
  }
  
  // const mapDispatchToProps = () => {
  const putActionsToProps = (dispatch) => {
    return {
      runNewApp: bindActionCreators(runNewApp, dispatch),
      runCalculatorApp: bindActionCreators(runCalculatorApp, dispatch)
    }
  }

export default connect(putStateToProps, putActionsToProps)(App)