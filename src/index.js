import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';

//IMPORTING COMPONENTS
import WindowWrapper from './components/WindowWrapper/WindowWrapper'
import Calculator from './components/WindowWrapper/inWindowApps/Calculator/Calculator'

//REDUX INITIALIZATION
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import rootReducer from './store/reducers'

// import {runNewApp} from './redux/pageActions';

const store = createStore( rootReducer );

ReactDOM.render(
    <Provider store={store}>
      <App />
    </Provider>,
  document.getElementById('root')
);